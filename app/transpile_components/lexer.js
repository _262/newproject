(function(trans) {
  /**
  * Это лексический анализатор. Здесь должен быть ваш код вместо данной затычки!
  * @param {string} inputCode - строка с кодом программы на входном языке
  * @return {Array} - массив распознанных лексем
  */
 trans.TransProto.lexer = function (inputCode) {

   var lex = this.lexTable; //Таблица лексем

/*
   this.arrayOfSymbols = {}; // массив символов, куда будут помещаться ссылки на массивы констант и идентификаторов
   this.lexTable = trans.getJSON(lexTablePath, this.arrayOfSymbols); // Таблица лексем, отпарсенная в JS-объект
   this.lexArray = []; // Массив найденных лексем
   this.errors = []; // Массив ошибок
*/
   for(var i in this.arrayOfSymbols){
     this.arrayOfSymbols[i] = [];
   }
   

   var cur_str = 1; //Счетчик строк, для ошибок
   var cur_colon = 1 //Счетчик столбцов
   while(inputCode.length != 0){

     var flag = false; //Флаг, что лексема найдена
     for( var key in lex){
       
       //Создаем регулярное выражение
       var reg = new RegExp(lex[key].regex);
       var match = reg.exec(inputCode); //проверяем на совпадение с регуляркой

       //Если найдена лексема..
       if(match && match.length != 0){
         ///////////////////////////////////////
         //*****Нужно ли пропустить лексему*****//    
         if(lex[key].skip){
           flag =true;
           inputCode = inputCode.replace(reg, "");//Удаляем пробелы
           // cur_colon+=match[0].length;



           //Если конец строки, запоминаем строкуномер строки
           for(var i=0; i< match[0].length; i++)
             if(match[0][i] == "\n"){
               cur_str++;    
               cur_colon=-1;

               //нужно ли в конце строки добавлять ";" в список лексем ? 
             }
         //*****Если лексема не индификатор и не константа*****//
         }else if(lex[key].list){
           //Определяем какая именно лексема
           for( var nameList in lex[key].list){
             for(var subname in lex[key].list[nameList]){
               
               var name_lex = lex[key].list[nameList][subname];//Помещаем n-ю лексему из таблицы лексем в перемену
               //Найденная лексема совпала совпала с лексемой из таблицы
               if(name_lex == match[0]){
                 flag = true;
                 this.lexArray.push([name_lex, cur_str, cur_colon]); //Заносим в список лексем
                 inputCode = inputCode.replace(reg, ""); //Удаляем из исходного текста найденую лексему
                 break;
               }
             }
           }
         }else{  //значит это константа или индификатор
           //Если она уже была, то просто просто запоминаем указатель на нее
           var const_tab = this.arrayOfSymbols[lex[key].link]; 
           for(var i =0; i < const_tab.length; i++){
             if(const_tab[i][1]== match[0]){ //Если найденная лексема уже есть в списке
               this.lexArray.push([lex[key].link, i]); //добавляем в таблицу лексем указатель
               flag = true;
               break;
             }
           }
           //Если констаная или индификатор не был записан ранее
           //Запоминаем её и добавляем в массив лексем указатель на него
           if(!flag){
             const_tab.push([const_tab.length, match[0]]); //Запоминаем лексему в таблице констант
             this.lexArray.push([lex[key].link, const_tab.length-1,  cur_str, cur_colon]); //Добавляем в массив лексем указатель на неё
             flag = true;
           }
           inputCode = inputCode.replace(reg, ""); //Удаляем лексему из текста, переходим к следующей
         }
         ///////////////////////////////////////

     }
     //Лексема не найдена, проверяем следующую легуряку
     if(flag){
       cur_colon+=match[0].length;
       
        break;
     }
     
   }
   /*
     Вывод сообщения о том что не удалось распознать
     лексему и занесения не распознанной лексемы в 
     массив ошибок
   */
   if(!flag){
     console.log("Error in line " + cur_str+", colon "+cur_colon); //Вывод номера строки где не удалось распознать лексему
     console.log(inputCode);//Вывод кода, начиная с нераспознанной лексемы
 
     //занесения не распознанной лексемы в массив ошибок
     var lex_error = inputCode.split(/\s+/);
     this.errors.push(lex_error[0], cur_str, cur_colon);
     inputCode = inputCode.replace(lex_error[0], "");
   }
 }
 //Возвращаем объект, с массивами лексем, констант, нераспознанных лексем
 return {
  "Lexems": this.lexArray,
  "Constants": this.arrayOfSymbols,
  "Error lexems": this.errors

 };
}

})(window.trans || (window.trans = {}));